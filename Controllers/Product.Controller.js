const createError = require("http-errors");
const { default: mongoose } = require("mongoose");
const Product = require('../Models/Product.model');

 module.exports = {
     getAllProducts:async (req,res,next) =>{
        try {
          const result = await Product.find({},{__v :0});
           res.json({result});
        } catch (error) {
            console.log(error.message)
        }
    },

    findProductById: async (req,res,next) =>{
        const id = req.params.id;
    
        try {
          const product = await Product.findById(id);
         // const product = await Product.findOne({_id:id});
          if(!product){
            throw createError(404,"Product does not exist");
          }  
           res.json({product});  
        } catch (error) {
            console.log(error.message);
            if(error instanceof mongoose.CastError){
                next(createError(400, "Invalid Product id"));
               return;
            }
            next(error);
        }
    },

    createNewProduct: async (req,res,next) =>{
        try {
            const product = new Product(req.body);
            const result = await product.save();
            res.json({result});
        } catch (error) {
            console.log(error.message);
            if(error.name === 'ValidationError'){
                next(createError(422, error.message));
                return;
            }
            next(error);
        }
        // console.log(req.body);
    
        // const product = new Product({
        //     name: req.body.name,
        //     price: req.body.price
        // });
    
        // product.save()
        // .then(result =>{
        //     console.log(result);
        //     res.send(result);
        // })
        // .catch(err =>{
        //     console.log(err.message);
        // })
    },

    deleteProduct: async (req,res,next) =>{
        const id = req.params.id;
        try {
          const result = await Product.findByIdAndDelete(id);
          
          if(!result){
              throw createError(404,"Product does not exist");
          }
   
          res.json({result});
        } catch (error) {
            console.log(error.message);
   
            if(error instanceof mongoose.CastError){
                next(createError(400, "Invalid Product id"));
               return;
            }
            next(error);
        }
   },

   updateProduct: async (req,res,next) =>{
        try { 
        const id = req.params.id;
        const updates = req.body;
        const options = {new: true};
        
        const result = await Product.findByIdAndUpdate(id,updates,options);

        if(!result){
            throw createError(404, "Product does not exist");
        }
        res.json({result});
        } catch (error) {
            console.log(error.message);
            if(error instanceof mongoose.CastError){
                return next(createError(400, "Invalid Product id  "));
            }

            next(error);
        }
    },
 }