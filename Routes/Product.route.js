const express =  require("express");
const router = express.Router();

const ProductController = require('../Controllers/Product.Controller');

//get all the products
router.get('/', ProductController.getAllProducts);

//Get by id
router.get('/:id', ProductController.findProductById);

//add the product
router.post('/post_product',ProductController.createNewProduct);

//delete product
router.delete('/delete_product/:id',ProductController.deleteProduct);

//update a product
router.patch('/update_product/:id',ProductController.updateProduct);


module.exports = router